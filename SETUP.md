# Basic setup

- composer install
- setup env file
- php spark shield:setup
- php spark migrate --all

## References

- [https://codeigniter.com]
- [https://codeigniter4.github.io/shield/getting_started/install/]

## Codeigniter 4

Version 4.4.3
